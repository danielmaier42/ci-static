#!/bin/bash

# Update Repositories
apt-get update

# Install Dependencies
apt-get install -y git curl zip unzip software-properties-common locales

# Define Localization
locale-gen en_US.UTF-8 && export LANG=en_US.UTF-8

# Install Composer
php -r "copy('https://getcomposer.org/installer', '/tmp/composer-setup.php');"
php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer

# Install Project via Composer
composer install --prefer-dist