#!/bin/bash

VERSION=`date +%s`

HOST=$1
PORT=$2
USER=$3
COMPOSER=$4
DOCUMENT_ROOT=$5

if [ -z "$PORT" ];
then
    PORT="22"
fi

echo "Deploying version $VERSION..."
echo "- Server: $HOST"
echo "- Port: $PORT"
echo "- User: $USER"
echo "- Composer: $COMPOSER"
echo "- Root: $DOCUMENT_ROOT"

echo "Rechecking SSH Identies:"
ssh-add -L

if [ -z "$HOST" ]; then
    >&2 echo "Host must be set!"
    exit -1
fi

if [ -z "$USER" ]; then
    >&2 echo "User must be set!"
    exit -1
fi

if [ -z "$COMPOSER" ]; then
    >&2 echo "Composer path must be defined!"
    exit -1
fi

if [ -z "$DOCUMENT_ROOT" ]; then
    >&2 echo "Document Root must be defined!"
    exit -1
fi

ssh "$USER"@$"$HOST" -p "$PORT" -o StrictHostKeyChecking=no -tt << EOF
    if [ ! -d "$DOCUMENT_ROOT" ]; then
        mkdir -p "$DOCUMENT_ROOT"
    fi

    if [ ! -d "$DOCUMENT_ROOT" ]; then
        >&2 echo "Document Root does not exist and could not create it neither"
        exit -1
    fi

    cd "$DOCUMENT_ROOT"
    
    mkdir -p "deployment/$VERSION/"

    if [ ! -d "deployment/$VERSION/" ]; then 
        >&2 echo "Could not create Deployment Path for Version $VERSION"
        exit -1 
    fi

    git clone $CI_REPOSITORY_URL "deployment/$VERSION/"

    if [ $? -ne 0 ]; then
        cd "$DOCUMENT_ROOT"
        rm -rf "deployment/$VERSION/"
        >&2 echo "Could not clone Git"
        exit -1
    fi

    cd "deployment/$VERSION/"
    git checkout $CI_COMMIT_REF_NAME

    if [ $? -ne 0 ]; then
        cd "$DOCUMENT_ROOT"
        rm -rf "deployment/$VERSION/"
        >&2 echo "Could checkout $CI_COMMIT_REF_NAME"
        exit -1
    fi

    eval "$COMPOSER install"

    if [ $? -ne 0 ]; then
        cd "$DOCUMENT_ROOT"
        rm -rf "deployment/$VERSION/"
        >&2 echo "Cannot install via Composer"
        exit -1
    fi

    cd "$DOCUMENT_ROOT"

    if [ -D "active" ]; then
        mkdir -p "archives"
        mv "active" "archives/$VERSION"
    fi

    if [ -L "active" ]; then
        rm "active"
    fi

    ln -s "deployment/$VERSION/" "active"

    exit
EOF

exit $?