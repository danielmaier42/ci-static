<!DOCTYPE html>
<html>
    <head>
        <title>Static Test</title>
        <style>
            body {
                background-color: #f0f0f0;
                color: #111111;
            }

            h1 {
                font-size: 2em;
                font-weight: bold;
            }

            p {
                font-style: italic;
            }
        </style>
    </head>
    <body>
        <h1>Static Headline</h1>
        <p>Suspendisse sed enim eros. Pellentesque sit amet lobortis mi. Aenean molestie feugiat velit, a congue libero. Nulla viverra pharetra porta. Integer semper egestas mi, eu bibendum orci. Pellentesque rhoncus tempus bibendum. Curabitur pretium felis id libero sagittis, elementum scelerisque sapien convallis. Morbi sem nisl, mollis tempor rhoncus nec, volutpat at magna. Aenean convallis quis sapien sed luctus. Donec aliquam sed metus mollis lobortis. Nunc laoreet erat a augue aliquam ultricies. Suspendisse blandit placerat augue, quis efficitur quam pellentesque eu. Cras vehicula in felis nec malesuada. Nam auctor diam nibh, vel dapibus risus pretium a.</p>
</html>
