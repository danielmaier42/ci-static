<?php

namespace App;

class BasicClass
{
    protected $testProperty;

    public function __construct($testArgument)
    {
        $this->testProperty = $testArgument;
    }

    public function getTestProperty()
    {
        return $this->testProperty;
    }
}
