<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\BasicClass;

class BasicClassTest extends TestCase
{
    public function testIsInitializedCorrectly()
    {
        $basicInstance = new BasicClass('foobar');

        $this->assertEquals(
            'foobar',
            $basicInstance->getTestProperty()
        );
    }
}
