# TYPO3 Continious Integration

[![build](https://gitlab.com/danielmaier42/ci-static/badges/master/pipeline.svg)](https://gitlab.com/danielmaier42/ci-static/commits/master)

This is a test repository for TYPO3 in combination with GitLab Continious Integration.

## CI Variables

Variable | Example | Usage
--- | --- | ---
`SSH_PRIVATE_KEY` | `-----BEGIN RSA PRIVATE KEY----- MIIJ...` | This Private Key will be used inside CI Docker image. So you should allow the corresponding Public Key access to all this repository and needed Servers.
`STAGING_URL` | `staging.example.org` | Public Url for Staging Website. Used for Environment Declaration.
`STAGING_HOST` | `8.8.8.8` | Url or IP Address of Staging SSH Server
`STAGING_PORT` | `22` | SSH Port for Connecting to Staging Server
`STAGING_USER` | `deploy` | SSH User used for Automated Deployment on Staging Server.
`STAGING_COMPOSER` | `/usr/bin/composer` | Specify the Composer executable path.
`STAGING_DOCUMENT_ROOT` | `/var/www/html/domain.com` | This should be the base path for our deployment. (Not the same as the Webservers Document Root)
`PRODUCTION_URL` | `production.example.org` | Public Url for Production Website. Used for Environment Declaration.
`PRODUCTION_HOST` | `8.8.8.8` | Url or IP Address of Production SSH Server
`PRODUCTION_PORT` | `22` | SSH Port for Connecting to Production Server
`PRODUCTION_USER` | `deploy` | SSH User used for Automated Deployment on Production Server.
`PRODUCTION_COMPOSER` | `/usr/bin/composer` | Specify the Composer executable path.
`PRODUCTION_DOCUMENT_ROOT` | `/var/www/html/domain.com` | This should be the base path for our deployment. (Not the same as the Webservers Document Root)